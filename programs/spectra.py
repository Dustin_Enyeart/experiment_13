import math as ma
import statistics as stat
import matplotlib.pyplot as plt
import scipy.optimize as optim

import lib


domain = list(range(8192))


if __name__ == '__main__':

    '''
    Plot the colbalt spectra at different energies.
    '''
    low = 4000
    up = 6001
    counts = lib.get_counts('co_60_spectrum_2000v')
    plt.plot(domain[low:up], counts[low:up], label='2000 V (recommended)')
    counts = lib.get_counts('co_60_spectrum_1500v')
    plt.plot(domain[low:up], counts[low:up], label='1500 V')
    counts = lib.get_counts('co_60_spectrum_1000v')
    plt.plot(domain[low:up], counts[low:up], label='1000 V')
    plt.xlabel('channel')
    plt.ylabel('count')
    plt.ylim(0)
    plt.legend()
    plt.show()


    '''
    Resolution.
    '''
    resolution = 4.99/1322.12
    print(f'The resolution of the 1332 keV peak is {resolution}.')

    voltages = [1000, 1500, 2000]
    fwhms = [9.04, 5.82, 4.99]
    fwtms = [15.29, 11.66, 9.52]
    ratios = [fwtm/fwhm for fwhm, fwtm in zip(fwhms, fwtms)]
    plt.plot(voltages, ratios)
    plt.xlabel('voltages (V)')
    plt.ylabel('ratio of FWTM to FWHM')
    plt.show()


    '''
    Plot the spectra of all three sources together.
    '''
    counts = lib.get_counts('calibration_spectrum')
    plt.plot(domain, counts)
    plt.xlabel('channel')
    plt.ylabel('count')
    plt.ylim(0)
    plt.xlim(0)
    plt.show()

    '''
    Plot the FWHM as a function of energy.
    '''
    energies_ba = [80.92, 276.43, 302.90, 356.06, 383.89]
    energies_cs = [661.59]
    energies_co = [1173.19, 1332.55]
    energies_na = [510.65, 1274.89]
    fwhms_ba = [1.87, 2.20, 2.22, 2.37, 2.40]
    fwhms_cs = [3.25]
    fwhms_co = [4.81, 5.48]
    fwhms_na = [3.88, 3.99]
    plt.scatter(energies_ba, fwhms_ba, label='Ba-133')
    plt.scatter(energies_cs, fwhms_cs, label='Cs-137')
    plt.scatter(energies_co, fwhms_co, label='Co-60')
    plt.scatter(energies_na, fwhms_na, label='Na-22', color='yellow')
    plt.xlabel('energy (keV)')
    plt.ylabel('FWHM')
    plt.xlim(0)
    plt.ylim(0)
    plt.legend()
    plt.show()

    '''
    Energy callibration
    '''
    channels = [321.94, 1104.14, 1210.02, 1422.69, 1534.04, 2644.97, 4691.38,
                5328.79]
    energies = energies_ba + energies_cs + energies_co

    def to_fit(x, a, b):
        return a*x + b
    params = optim.curve_fit(to_fit, channels, energies)[0]
    def calibration(channel):
        return params[0]*channel + params[1]

    predicted_energies = [calibration(channel) for channel in channels]
    plt.scatter(channels, energies, color='red', label='data')
    plt.plot(channels, predicted_energies, label='fit')
    plt.scatter([2041.15, 5098.15], [511., 1274.5],
                color='yellow', label='Na-22') #Na-22
    plt.xlabel('channel')
    plt.ylabel('energy (keV)')
    plt.xlim(0)
    plt.legend()
    plt.show()


    '''
    Peak-to-Compton ratio
    '''
    counts = lib.get_counts('cs_137_spectrum')
    edge_counts = 0
    for channel in domain:
        energy = calibration(channel)
        if energy >= 358 and energy <= 382:
            edge_counts += counts[channel]
    ratio = edge_counts/661.66
    print(f'The peak-to-Comption ration is {ratio}.')


    '''
    Spectrum of unknown.
    '''
    counts = lib.get_counts('sample_25_spectrum')
    counts = counts[100:]
    domain = [calibration(channel) for channel in domain[100:]]
    plt.plot(domain, counts)
    plt.xlabel('energy (keV)')
    plt.ylabel('count')
    plt.xlim(0)
    plt.ylim(0)
    plt.show()
